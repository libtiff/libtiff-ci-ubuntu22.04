# syntax = docker/dockerfile:1.0-experimental
FROM ubuntu:22.04
MAINTAINER tiff@lists.osgeo.org

# Install required build dependencies
RUN apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y -qq --no-install-recommends \
    autoconf \
    autoconf-archive \
    automake \
    build-essential \
    cmake \
    curl \
    ca-certificates \
    doxygen \
    libtool \
    git \
    graphviz \
    locales \
    ninja-build \
    pkg-config \
    python3-sphinx \
    python3-sphinx-rtd-theme \
    zip \
    unzip \
    wget \
    libgtest-dev \
    libjpeg8-dev \
    libjbig-dev \
    liblzma-dev \
    zlib1g-dev \
    libdeflate-dev \
    libzstd-dev \
    libwebp-dev \
    liblerc-dev \
  && locale-gen en_US.UTF-8

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
